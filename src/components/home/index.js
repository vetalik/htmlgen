import { html, render } from 'lit-html';
import { getData } from "../data-service";
import { getWidgets } from "../widget";

export function makeBody() {
  getData('http://localhost:3001/data').then(data => {
    render(html`
      <body>
        <div class="widgets">${getWidgets(data.widgets)}</div>
      </body>
    `, document.body);
  });
}
