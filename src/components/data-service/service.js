export async function getData(url) {
  const body = await fetch(url).then(data => data.json());
  return body;
}
