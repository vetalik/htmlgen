import { html } from "lit-html";
import { repeat } from "../../../node_modules/lit-html/lib/repeat";

const formTemplate = (params, items) => {
  return html`
    <form id=${params.id}>
      <fieldset>
        <legend>${params.name}</legend>
        ${repeat(items, (item) => item)}
      </fieldset>
    </form>
  `;
};

const sectionTemplate = (params, items) => {
  const range = [...Array(params.columns).keys()];
  return html`
    <section>
      <header>${params.header}</header>
      <div class="columns">
        ${repeat(range, (col) => (
          html`
            <div class="column">
              ${repeat(items, (item) => item)}
            </div>
          `))
        }
      </div>
    </section>
  `;
};
const currencyTemplate = (params, items) => {
  const precision = params.precision ? 0.01 * params.precision : 0.01;
  return html`
    <label>${params.label}
      <div class="currency-field">
        <span class="currencyinput">${params.symbol}
          <input type="number" value="${params.value}" id="${params.id}" min="${precision}" step="${precision}"/>
        </span>
      </div>
    </label>
  `;
};
const inputTemplate = (params, items) => html`
    <label class="text-input">${params.label}
      <input type="text" value="${params.value}" required="${params.required}" />
    </label>
  `;
const emptyDiv = html`<div></div>`;

const selectTemplate = (data) => (items = []) => {
  switch (data.type) {
    case "form":
      return formTemplate(data, items);
    case "input":
      return html`${inputTemplate(data, items)}`;
    case "section":
      return html`${sectionTemplate(data, items)}`;
    case "currency":
      return html`${currencyTemplate(data, items)}`;
    default:
      return emptyDiv;
  }
};

export const widget = (data) => {
  let selectedWidgets = [];
  if (Array.isArray(data.items)) {
    selectedWidgets = data.items.map(item => widget(item));
  }
  const selectedWidget = selectTemplate(data);
  return html`${selectedWidget(selectedWidgets)}`;
};

export const getWidgets = (widgets) => {
  return html`${repeat(widgets.map(widget), el => el)}`;
};
