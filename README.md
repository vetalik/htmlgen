# reqiremets
npm version >=8.9

# preparation
npm i
or
yarn install

# develoment
to run dev server in watch mode with data file in parallel
`npm run dev`

# build

to build static files into dist folder
`npm run build`

to run build from dist folde
`start-prod`


# Notes

For html rendering is used lit-html. library that uses tagged template literals
For CSS i'm using Post-CSS 

Built files does not require any external dependencies. 
